
#include <iostream>
#include <rdi_image.h>
#include <rdi_geometry.h>
#include <queue>
#include <vector>
using namespace std;
using namespace RDI;
using namespace cimg_library;
using namespace RDI::geometry;
int main()
{
    Rectangle<> r1 {{100,100},{200,200}};
    Rectangle<> r2 = {{50,150},{120,220}};
    cout<< r1.a <<" "<<r1.b<<" "<<r1.c<<" "<<r1.d<<endl;
    cout<< r2.a <<" "<<r2.b<<" "<<r2.c<<" "<<r2.d<<endl;
    auto l1 = Line<>(r1.a,r1.b);
    auto l2 = Line<>(r2.a,r2.b);
    cout <<Line<>(r1.a,r1.b).height()<<endl;
    cout <<Line<>(r2.a,r2.b).height()<<endl;

    // cout<<l.height() <<" "<<l2.height()<<endl;
    cout<<geometry::get_overlapped_height(l1,l2)<<endl;
    cout<<geometry::get_height_overlap_ratio(l1,l2);
    return 0;
    CImage<> img("images/score_rects.png");
    cout<<img(0,0,0)<<" "<< img(0,0,1) <<" "<< img(0,0,2)<<endl;
    // img = img.resize(img.width()*4, img.height()*4);

    auto w = img.width(), h = img.height();

    // img.resize(w/4,h/4);
    vector<vector<int>> visited(w, vector<int>(h, 0));

    auto bfs = [&](int i, int j) {
        queue<pair<int, int>> q;
        q.push({i, j});
        int min_x, max_x, min_y, max_y;
        min_x = min_y = std::max(w, h) + 1;
        max_x = max_y = 0;
        Point2D<> min_x_p, max_x_p, min_y_p, max_y_p;

        while (!q.empty())
        {
            auto front = q.front();
            q.pop();
            auto x = front.first;
            auto y = front.second;
            
            if (x < 0 or x > w or y < 0 or y > h or (int)img(x, y, 0, 0) < 10 or visited[x][y] == 1)
            {
                // cout<<x<<" "<<y<<endl;
                continue;
            }
            q.push({x + 1, y});
            q.push({x - 1, y});
            q.push({x, y + 1});
            q.push({x, y - 1});
            visited[x][y] = 1;

            Point2D<> t(x, y);
            if (t.x < min_x)
            {
                min_x = t.x;
                min_x_p = {min_x, t.y};
            }
            if (t.x > max_x)
            {
                max_x = t.x;
                max_x_p = {max_x,t.y};
            }
            if (t.y < min_y)
            {
                min_y = t.y;
                min_y_p = {t.x,min_y};
            }
            if (t.y > max_y)
            {
                max_y = t.y;
                max_y_p = {t.x,max_y};
            }
        }
        return vector<Point2D<>>{max_y_p, max_x_p, min_y_p, min_x_p};
    };

    // CImgDisplay display(img, "Rectangels");

    vector<vector<Point2D<>>> bounds;

    for (auto i = 0ul; i < w; ++i)
    {
        for (auto j = 0ul; j < h; ++j)
        {
            // cout<< (int)img(i,j,0,0)<<" ";
            if (visited[i][j] == 0 and (int)img(i, j, 0, 0) > 200)
            {
                bounds.emplace_back(bfs(i, j));
            }
        }
        // cout<<endl;
    }
    vector<Rectangle<>> rects;
    for (auto &i : bounds)
    {
        rects.emplace_back(
            Rectangle<>({i[3].x, i[2].y}, {i[1].x, i[0].y}));
    }

    const unsigned char red[] = {255, 0, 0};
    img = img.resize(w * 4, h * 4);

    for (auto &quad : rects)
    {
        img = img.draw_rectangle(quad.a.x*4 , quad.a.y*4 , (quad.c.x+1)*4 , (quad.c.y+1)*4 , red, 0.5, ~0U);
        // cout<<quad<<endl;
        // cout << quad.a.x << " " << quad.a.y << ":" << quad.c.x << " " << quad.c.y << endl;
    }
    // img.display(display);
    // // img.draw_text(200, 200, "you suck", red, 0, 0.8, 24);
    // display.wait();

    img.save("out.png");
    return 0;
}
