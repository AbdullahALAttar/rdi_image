
#ifndef RDI_IMAGE_H
#define RDI_IMAGE_H
#include <CImg.h>
#include <experimental/string_view>
#include <rdi_geometry.h>

namespace RDI
{

template <typename T = unsigned char>
class CImage : public cimg_library::CImg<T>
{
    using _img = cimg_library::CImg<T>;

  public:
    std::string image_name;


    CImage(std::experimental::string_view path) : _img(path.data()),
                                   image_name(path) {}
    CImage(_img im) : _img(im) {}
    template<typename R> 
    CImage<T> get_crop(const geometry::Rectangle<R> &q)
    {
        return _img::get_crop(q.a.x, q.a.y, q.c.x, q.c.y);
    }
};

} // namespace RDI
#endif