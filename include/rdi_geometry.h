
#ifndef RDI_GEOMETRY_H
#define RDI_GEOMETRY_H
#include <assert.h>
#include <cmath>
#include <ostream>
namespace RDI
{
namespace geometry
{
    template <typename T = int>
    struct Point2D
    {
	T x, y;
	Point2D() : x(0), y(0) {}
	Point2D(T x, T y) : x(x), y(y) {}
	Point2D operator+(Point2D p) { return { x + p.x, y + p.y }; }
	Point2D operator-(Point2D p) { return { x - p.x, y - p.y }; }
	Point2D operator*(T d) { return { x * d, y * d }; }
	Point2D operator/(T d) { return { x / d, y / d }; }
	friend bool operator==(const Point2D& a, const Point2D& b)
	{
	    return a.x == b.x && a.y == b.y;
	}
	friend bool operator!=(const Point2D& a, const Point2D& b)
	{
	    return !(a == b);
	}
	friend std::ostream& operator<<(std::ostream& os, const Point2D& p)
	{
	    return os << "(" << p.x << ", " << p.y << ")";
	}

	inline T dist(const Point2D& p)
	{
	    return std::sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y));
	}
	void rotate(double angle)
	{
	    x = x * std::cos(angle) - y * std::sin(angle);
	    y = x * std::sin(angle) + y * std::cos(angle);
	}
	Point2D rotate_around(const Point2D& p, double a)
	{
	    return { (x - p.x) * std::cos(a) - (y - p.y) * std::sin(a) + p.x,
		     (x - p.x) * std::sin(a) + (y - p.y) * std::cos(a) + p.y };
	}
    };
    ///  a -------------- d
    ///    |            |
    ///    |            |
    ///  b -------------- c

    template <typename T = int>
    struct Rectangle
    {

	Point2D<T> a, b, c, d;
	Rectangle() = default;
	// incase its just a rectangle
	Rectangle(Point2D<T> top_left, Point2D<T> bottom_right)
	    : a(top_left), b({ top_left.x, bottom_right.y }), c(bottom_right),
	      d({ bottom_right.x, top_left.y })
	{
	}

	Rectangle(Point2D<T> a, Point2D<T> b, Point2D<T> c, Point2D<T> d)
	    : a(a), b(b), c(c), d(d)
	{
	}

	friend std::ostream& operator<<(std::ostream& os, const Rectangle& r)
	{
	    return os << "{" << r.a << ", " << r.c << "}";
	}
	inline T width() const { return dist(a, b); }
	inline T height() const { return dist(a, b); }
	Point2D<T> center() const { return (a + c) / 2; }
	void rotate_around_center(double angle)
        {
	    auto cent = center();
	    a         = a.rotate_around(cent, angle);
	    b         = b.rotate_around(cent, angle);
	    c         = c.rotate_around(cent, angle);
	    d         = d.rotate_around(cent, angle);
        }

	bool intersects(const Rectangle& other) const noexcept
        {
	    return ((this->a.x < other.c.x) && (this->c.x > other.a.x)
	            && (this->a.y < other.c.y) && (this->c.y > other.a.y));
        }
    };

    template <typename T = int>
    struct Line
    {
	Point2D<T> a, b;
	Line(Point2D<T> a, Point2D<T> b) : a(a), b(b) {}
	inline T height() const { return dist(a, b); }
	friend std::ostream& operator<<(std::ostream& os, const Line& l)
	{
	    return os << "{" << l.a << ", " << l.b << "}";
	}
	inline T max_y() const noexcept { return std::max(a.y, b.y); }
	inline T min_y() const noexcept { return std::min(a.y, b.y); }
    };

    template <typename T = int>
    static inline T dist(const Point2D<T>& a, const Point2D<T>& b) noexcept
    {
	return std::sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
    }
    template <typename T = int>
    inline double get_height_overlap_ratio(const Line<T>& l1,
                                           const Line<T>& l2) noexcept
    {
	return get_overlapped_height(l1, l2)
	    / (1.0 * std::min(l1.height(), l2.height()));
    }
    template <typename T = int>
    inline T get_overlapped_height(const Line<T>& l1,
                                   const Line<T>& l2) noexcept
    {
	// this works if the origin is at top left
	return std::min(l1.max_y(), l2.max_y())
	    - std::max(l1.min_y(), l2.min_y());
    }
    template <typename T = int>
    inline Rectangle<T> merge_rectangles(const Rectangle<T>& r1,
                                         const Rectangle<T>& r2)
    {
	return Rectangle<T>{
	    { std::min(r1.a.x, r2.a.x), std::min(r1.a.y, r2.a.y) },
	    { std::max(r1.c.x, r1.c.y), std::max(r1.c.y, r1.c.y) }
	};
    }
} // namespace geometry
} // namespace RDI

#endif
